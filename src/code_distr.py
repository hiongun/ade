#!/bin/env python
# -*- coding: utf-8 -*-

import sys
import math

def sigmoid(x):
#{
    """시그모이드 함수, 너무 큰수, 너무 작은 수에서는 수치 오류 -> 근사치로 수렴"""
    if (x > 700.0): return 1.0
    if (x < -700.0): return 0.0
    return 1.0 / (1.0 + math.exp(-x))
#}

def compute_stats(vals):
#{
    """과거 데이터(vals)에 대한 통계적 분포값 계산하는 로직, "합, 평균, 표준편차, MIN, MAX" 계산"""

    n_vals = len(vals)

    if n_vals < 1:
        return (0.0, 0.0, 0.0, 0.0, 0.0)

    if n_vals < 2:
        return (float(vals[0]), float(vals[0]), 0.0, float(vals[0]), float(vals[0]))

    # -------------------------------------------------------------
    # 알고리듬 출처: Donald Knuth's
    # "The Art of Computer Programming, Volume 2: Seminumerical Algorithms",
    # section 4.2.2
    #
    # M(1) = x(1), M(k) = M(k-1) + (x(k) - M(k-1)) / k
    # S(1) = 0, S(k) = S(k-1) + (x(k) - M(k-1)) * (x(k) - M(k))
    # for 2 <= k <= n,
    # Sigma = sqrt(S(n) / (n - 1)) ==> Equals to STDEVA(X1~Xn) in Excel
    # ------------------------------------------------------------

    M = float(vals[0]) # MEAN at K'th position
    S = 0.0            # STDEVP at K'th position

    MIN = M
    MAX = M

    for k in range(1, n_vals):
        X = float(vals[k])
        M_1 = M
        M = M + (X - M) / (k+1.0)
        S = S + (X - M_1) * (X - M)
        if (X < MIN): MIN = X
        if (X > MAX): MAX = X

    S = math.sqrt(S / ((float(n_vals)) - 1.0))
    return (M*float(n_vals), M, S, MIN, MAX) # (Sum, Mean, Sig, Min, Max)
#}




if __name__ == "__main__":
#{
    stat = {}
    fields = {}
    fp = open("./code_stat.out", "r")
    for line in fp.xreadlines():
    #{
        # A1471 TCount:957, TSum:1432, TMean:1.50, TSig:1.22, TMin:1.00, TMax:16.00, MCount:6, MSum:1432, MMean:238.67, MSig:494.71, MMin:5.00, MMax:1247.00
        row = line.strip().split(" ")
        if not stat.has_key(row[0]):
            stat[row[0]] = {}
        for i in range(1, len(row)):
            if row[i][-1:] == ',':
                row[i] = row[i][0:-1]
            kv = row[i].split(":")
            stat[row[0]][kv[0]] = float(kv[1])

            if not fields.has_key(kv[0]):
                fields[kv[0]] = [float(kv[1])]
            else:
                fields[kv[0]].append(float(kv[1]))
    #}
    fp.close()

    for f in fields.keys():
    #{
        Vect = fields[f]
        # print "Vect: ", Vect
        (Sum, Mean, Sig, Min, Max) = compute_stats(Vect)

        fields[f] = {}
        fields[f]['Sum'] = Sum
        fields[f]['Mean'] = Mean
        fields[f]['Sig'] = Sig
        fields[f]['Min'] = Min
        fields[f]['Max'] = Max

        print "field:%s, Sum:%.2f, Mean:%.2f, Sig:%.2f, Min:%.2f, Max:%.2f" % (f, Sum, Mean, Sig, Min, Max)
    #}



    codes = {}
    for c in stat.keys():
    #{
        # print "%s " % c,
        Alphabet = ""
        for f in stat[c].keys():
        #{
            V = stat[c][f]

            VNorm = (V - fields[f]['Mean'])/fields[f]['Sig']

            if -1.0 < VNorm and VNorm <= 1.0:
               alphabet = 'N'
            elif -2.0 < VNorm and VNorm <= 2.:
               alphabet = 'C'
            elif -3.0 < VNorm and VNorm <= 3.0:
               alphabet = 'B'
            else:
               alphabet = 'A'

            VSigmoid = sigmoid(VNorm)
            # print "%s:%.1f/%.2f/%c, " % (f, VSigmoid, VNorm, alphabet),
            # print "%c " % (alphabet),
            Alphabet += alphabet
        #}


        if not codes.has_key(Alphabet):
            codes[Alphabet] = [c]
        else:
            codes[Alphabet].append(c)
    #}

    for alpha in codes.keys():
    #{
        print "%s: " % alpha, codes[alpha]
    #}
#}


