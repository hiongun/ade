#!/bin/env perl

MAIN:
{
    local(*D);
    if (opendir(D, ".")) {
        my @files = readdir(D);
        @files = sort {$a <=> $b} @files;
        foreach my $file (@files) {
            if ($file =~ /^\d+$/) {
                # print `cat $file | go run stat.go`;
                print `cat $file | python code_stat.py`;
            }
        }
        closedir(D);
    }
}
