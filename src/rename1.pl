local(*D);

opendir(D, ".");

while (my $file = readdir(D)) {
    if ($file =~ /^(2015.+)\.DAT\.DAT$/) {
        rename($file, "$1.DAT");
    }
}
