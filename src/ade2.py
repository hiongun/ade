#!/bin/env python
# -*- coding: utf-8 -*-

""" 일반화된 3Sigma에 의한 Anomaly Detector를 구현한 모듈 """

import collections # collections.OrderedDict
import string
import re
import datetime
import time
import math
import sys

def DEBUG_PRINT(msg):
    """ displays a warning message with filename and line number """
    fr = sys._getframe(1)
    print "(%s,%d) %s" % (fr.f_code.co_filename, fr.f_lineno, msg.strip())

class LimitedSizeDict(collections.OrderedDict):
#{
    """일정 사이즈 이상 커지지 않고, 새로운 값이 들어오면, 오래된 것을 지우는 딕트(dict) 구조체"""

    # 메모리에 과거 측정치를 기억해 두고, RandomAccess를 하면서 재계산에 사용

    # vals = LimitedSizeDict(size_limit=s_limit) # 사이즈 크기 제한 초기화 방법
    # vals[x] = y  (Assign방법)

    def __init__(self, *args, **kwds):
    #{
        self.size_limit = kwds.pop("size_limit", None)
        collections.OrderedDict.__init__(self, *args, **kwds)
        self._check_size_limit()
     #}

    def __setitem__(self, key, value):
    #{
        self._check_size_limit()
        collections.OrderedDict.__setitem__(self, key, value)
    #}

    def _check_size_limit(self):
    #{
        if self.size_limit is not None:
            while len(self) > self.size_limit:
                self.popitem(last=False)
    #}
#}


def unit2second(val):
#{
    """ 시간 표기를 초단위 수자로 바꾸는 함수 """
    # 12h, 60m, 30s 형태로 쓸 수 있음.
    m = re.search(r'(\d+)(\w+)', val)
    if m == None: return -1

    num = string.atoi(m.group(1), 10)
    unit = m.group(2)

    if unit == 's' or unit == 'S': num = num
    if unit == 'm' or unit == 'M': num = num * 60
    if unit == 'h' or unit == 'H': num = num * 60 * 60
    if unit == 'd' or unit == 'D': num = num * 60 * 60 * 24

    return num
#}

def test_unit2second():
#{
    assert(unit2second("1m") == 60)
    assert(unit2second("5h") == 60*60*5)
    assert(unit2second("5d") == 60*60*5*24)
#}

def YmdHMS2long(t_stamp): # 캘린더 시간을 유닉스 시간으로 변환
#{
    # dt = datetime.datetime.strptime(t_stamp,'%Y%m%d%H%M%S')
    t = time.mktime(time.strptime(t_stamp, '%Y%m%d%H%M%S'))
    return int(t)
#}

def long2YmdHMS(t_long):
#{
    return time.strftime("%Y%m%d%H%M%S", time.localtime(t_long))
#}


def test_YmdHMS2long():
#{
    assert(YmdHMS2long('20150924123049') == int("1443065449"))
#}

def test_long2YmdHMS():
#{
    assert(long2YmdHMS(int("1443065449")) == '20150924123049')
#}


def compute_stats(vals, smoothing=True):
#{
    """ 과거 데이터(vals)에 대한 통계적 분포값 계산하는 로직, (합, 평균, 표준편차, 최소, 최대) 계산' """
    # returns (Sum, Mean, Sigma, Min, Max)

    n_vals = len(vals)

    if n_vals < 1:
        return (0.0, 0.0, 0.0, 0.0, 0.0)

    if n_vals < 2:
        return (float(vals[0]), float(vals[0]), 0.0, float(vals[0]), float(vals[0]))

    # -------------------------------------------------------------
    # 알고리듬 출처: Donald Knuth's
    # "The Art of Computer Programming, Volume 2: Seminumerical Algorithms",
    # section 4.2.2
    #
    # M(1) = x(1), M(k) = M(k-1) + (x(k) - M(k-1)) / k
    # S(1) = 0, S(k) = S(k-1) + (x(k) - M(k-1)) * (x(k) - M(k))
    # for 2 <= k <= n,
    # Sigma = sqrt(S(n) / (n - 1)) ==> Equals to STDEVA(X1~Xn) in Excel
    # ------------------------------------------------------------

    M = float(vals[0]) # MEAN at K'th position
    S = 0.0            # STDEVP at K'th position

    MIN = M
    MAX = M

    for k in range(1, n_vals):
        X = float(vals[k])
        M_1 = M
        M = M + (X - M) / (k+1.0)
        S = S + (X - M_1) * (X - M)
        if (X < MIN): MIN = X
        if (X > MAX): MAX = X

    S = math.sqrt(S / ((float(n_vals)) - 1.0))

    if (smoothing): # 2Sigma 밖의 값을 2Sigma이내로 몰아넣어서, Smoothing을 함.
    #{
        sig = S
        mean = M

        M = float(vals[0])
        if M > (mean+2.0*sig): M = mean+2.0*sig
        if M < (mean-2.0*sig): M = mean-2.0*sig
        S = 0.0            # STDEVP at K'th position

        MIN = M
        MAX = M

        for k in range(1, n_vals):
            X = float(vals[k])
            if X > (mean+2.0*sig): X = mean+2.0*sig
            if X < (mean-2.0*sig): X = mean-2.0*sig
            M_1 = M
            M = M + (X - M) / (k+1.0)
            S = S + (X - M_1) * (X - M)
            if (X < MIN): MIN = X
            if (X > MAX): MAX = X

        S = math.sqrt(S / ((float(n_vals)) - 1.0))
    #}

    return (M*float(n_vals), M, S, MIN, MAX)
#}

def test_compute_stats():
#{
    vals = [1,2,3,4,5]
    (sum, mean, sig, min, max) = compute_stats(vals)
    print "vals: ", vals, ", sum: %.1f, mean: %.2f, sig: %.2f, min:%.2f, max:%.2f" % (sum, mean, sig, min, max)

    assert(sum == 15.0)
    assert(mean == 3.0)
    assert(1.58 < sig and sig < 1.59)
    assert(min <= 1.0)
    assert(max >= 5.0)

    vals = [1, 0, 1, 1, 2, 1, 4, 2, 1, 19]
    (sum, mean, sig, min, max) = compute_stats(vals, smoothing=True)
    print "vals: ", vals, ", sum: %.1f, mean: %.2f, sig: %.2f, min:%.2f, max:%.2f" % (sum, mean, sig, min, max)

    assert(27.50 <= sum and sum <= 27.60)
    assert(2.75 <= mean and mean <= 2.76)
    assert(4.26 < sig and sig < 4.27)
    assert(min <= 0.0)
    assert(max >= 14.5)
#}


def sigmoid(x):
#{
    """ 시그모이드 함수 """
    # 예외처리: 너무 큰수, 너무 작은 수에서는 수치 오류 -> 근사치로 수렴
    if (x > 700.0): return 1.0
    if (x < -700.0): return 0.0
    return 1.0 / (1.0 + math.exp(-x))
#}

def test_sigmoid():
#{
    print sigmoid(0.5)
    print sigmoid(1.0)
    print sigmoid(1.5)
    print sigmoid(2.0)
    print sigmoid(2.5)
    print sigmoid(600.0)
    print sigmoid(800.0)
    print sigmoid(20000.0)

    print sigmoid(-0.5)
    print sigmoid(-1.0)
    print sigmoid(-1.5)
    print sigmoid(-2.0)
    print sigmoid(-2.5)
    print sigmoid(-600.0)
    print sigmoid(-800.0)
    print sigmoid(-20000.0)
#}




class SimpleAnomalyDetector(): # 단일변수 Anomaly Detector 클래스
#{
    """특정 변수 하나에 대한 비정상 탐지를 하는 엔진"""

    # ------------------------------------------------------------------------
    # 변수명(name), 최대 데이터 보관기간(time_expire=30d)
    # 데이터 수집 빈도(frequency=5m), 데이터 관찰 윈도우(window_size=3)
    # 짧은 주기 (cycle_short=1d), 긴 주기 (cycle_long=7d)를 줘서
    # 모델을 커스터마이징 할 수 있음.
    # ------------------------------------------------------------------------
    def __init__(self, name="name", time_expire="30d", frequency="5m", window_size=5, cycle_short="24h", cycle_long="7d"):
    #{
        self.name = name
        self.time_expire = unit2second(time_expire) # 데이터 최대 보관 기간
        self.frequency = unit2second(frequency)     # 데이터 입력 주기
        self.window_size = window_size             # 과거 데이터를 바라보는 윈도우
        self.cycle_short = unit2second(cycle_short) # 짧은 사이클
        self.cycle_long = unit2second(cycle_long)   # 긴 사이클

        s_limit = int(self.time_expire / self.frequency)
        self.vals = LimitedSizeDict(size_limit=s_limit)   # 최대 저장용량이 제한된 딕트
    #}

    # 특정 순간에 값을 저장한다.
    def set(self, t, v): # t값에 unix timestamp가 와야 한다. 예: 1443065449
    #{
        self.vals[t] = v
    #}

    # 특정 순간의 값을 가져온다.
    def get(self, t): # t(unix timestamp)에 해당하는 값을 가져온다.
    #{
        try:
            return self.vals[t]
        except:
            return None
    #}


    # 해당 시점(t)에 과거 데이터를 기반으로 이상이 있는지를 판단한다.
    def detect(self, t):
    #{
        """ t시점에 값이, 과거 값에 대비해서, abnomal한 데이터인가? """
        # ---------------------------------------
        # returns (True, Type, SigmaLevel), Type은 3Sig 같은 룰 이름이 나온다
        # ---------------------------------------

        check_v = self.get(t)
        if check_v == None: # 현재 값이 Null이면 무시한다.
            return (False, None, 0.0)


        # 입력주기, 윈도우, 짧은 사이클, 긴 사이클 등을 감안하여,
        # 현재 시점 대비, 관찰할 과거 시점을 복수개 선정한다.
        ticks = []

        # DEBUG_PRINT("self.window_size: %d" % (self.window_size))

        for i in range(1, self.window_size+1):
            ticks.append(t - i*self.frequency)

        if self.cycle_short > 0:
            for i in range(1, self.window_size+1):
                for j in range(0, self.window_size):
                    ticks.append(t - i*self.cycle_short - j*self.frequency)

        if self.cycle_long > 0:
            for i in range(1, self.window_size+1):
                for j in range(0, self.window_size):
                    ticks.append(t - i*self.cycle_short - j*self.frequency)

        ticks.reverse() # 역순, 과거의 데이터에서 현재로 오면서 평균/표준편차 계산

        # 각 관찰 포인트별로, 해당 하는 값을 가져와서 벡터를 구성한다
        vals = []
        for tick in ticks:
            v = self.get(tick)
            if v != None:
                vals.append(v)

        if len(vals) < 9: # 관찰 포인트가 일정수 미만이면 판단을 유보한다.
            return (False, None, 0.0)


        # 과거의 참조값들을 기준으로 통계량(sum, mean, sig, min, max)를 구한다.
        (sum, mean, sig, min, max) = compute_stats(vals)

        if sig == 0.0:
            sig = 0.000000001
            if (float(check_v) - float(mean)) * (float(check_v) - float(mean)) < 2.0:
                return (False, None, 0.0)

        # DEBUG_PRINT("-")

        # 상한선 통과 체크
        if float(check_v) > mean + 3.5 * sig:
            print "%s t=%s vals:" % (self.name, long2YmdHMS(t)), vals, ", check_v:%d, mean:%.1f, sig:%.1f" % (check_v, mean, sig)
            return (True, '+3Sigma', (float(check_v)-mean)/sig)


        # 하한선 통과 체크
        if float(check_v) < mean - 3.5 * sig:
            print "%s t=%s vals:" % (self.name, long2YmdHMS(t)), vals, ", check_v:%d, mean:%.1f, sig:%.1f" % (check_v, mean, sig)
            return (True, '-3Sigma', (mean - float(check_v))/sig)

        return (False, '', 0.0)
    #}

    def get_stat(self):
    #{
        stat = "* name: %s\n" % (self.name)
        stat += "* time_expire: %d\n" % (self.time_expire)
        stat += "* frequency: %d\n" % (self.frequency)
        stat += "* window_size: %d\n" % (self.window_size)
        stat += "* cycle_short: %d\n" % (self.cycle_short)
        stat += "* cycle_long: %d\n" % (self.cycle_long)

        for k in self.vals.keys():
            stat += "* [%d: %d]\n" % (k, self.vals[k])
        stat += "."

        return "+OK STAT\n" + stat
    #}
#}



def test_SimpleAnomalyDetector():
#{
    adetect = SimpleAnomalyDetector()
    adetect.set(YmdHMS2long("20150321221200"), 0)
    print adetect.get_stat()
    adetect.detect(YmdHMS2long("20150321221200"))
#}

def test():
#{
    test_unit2second()
    test_SimpleAnomalyDetector()
    test_YmdHMS2long()
    test_long2YmdHMS()
    test_compute_stats()
    test_sigmoid()
#}

def load_machines(filename):
#{
    """이상탐지를 할 머신들의 이름을 로딩하다."""

    machines = []

    fp = open(filename, "r")
    for line in fp.xreadlines():
        machine = line.strip()
        machines.append(machine)

    fp.close()

    return machines
#}

#{
    if string.find(long2YmdHMS(t), "000000") > 0:
        DEBUG_PRINT("run detect_anomaly() %s" % long2YmdHMS(t))

    for machine in machines:
    #{
        anomaly_count = 0
        anomaly_sum = 0.0

        # DEBUG_PRINT("- machine:%s" % machine)

        for var in vars:
            key = machine + '.' + var

            # DEBUG_PRINT("- key:%s" % key)

            if detectors.has_key(key):
                (Anomaly, Type, SigmaLevel) = detectors[key].detect(t)

                if Anomaly:
                    anomaly_count += 1
                    anomaly_sum += sigmoid(SigmaLevel)
                    print " [detected] %s %s Anomaly:%d SigmaLevels=%.1f,Types=%s" % (machine, long2YmdHMS(t), Anomaly, SigmaLevel, Type)

        if anomaly_count >= 1:
            anomaly_mean = anomaly_sum / float(len(vars))
            print "DETECTED machine=%s, t=%s, count:%d, sum:%.2f, mean(sigmoid):%.2f" % (machine, long2YmdHMS(t), anomaly_count, anomaly_sum, anomaly_mean)
    #}
#}


def main():
#{
    machines = set([])
    codes = set([])

    detectors = {}

    prev_time = None
    for line in sys.stdin.xreadlines():
    #{
        t_ = line.strip().split(":")
        m_ = t_[1].split(".")
        c_ = m_[1].split(",")

        t = YmdHMS2long(t_[0])
        m = m_[0]
        c = c_[0]
        v = int(c_[1])

        #if m != "AMME060":
        #    continue

        #if c != "F6453":
        #    continue

        key = m + "." + c
        if not detectors.has_key(key):
            machines.add(m)
            codes.add(c)
            detectors[key] = SimpleAnomalyDetector(name=key, window_size=3)

        # DEBUG_PRINT("set %s=%d\n" % (key, v))
        detectors[key].set(t, v)

        if prev_time == None:
            prev_time = t
            continue

        if prev_time != t:
            detect_anomaly(detectors, machines, codes, prev_time)
            prev_time = t
    #}
#}


if __name__ == "__main__":
#{
    test()
    try:
        main()
    except Exception,e:
        print e
#}



