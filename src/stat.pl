#!/bin/env python
# -*- coding: utf-8 -*-

import math
import sys

def compute_stats(vals):
#{
    """과거 데이터(vals)에 대한 통계적 분포값 계산하는 로직, "합, 평균, 표준편차, MIN, MAX" 계산"""

    n_vals = len(vals)

    if n_vals < 1:
        return (0.0, 0.0, 0.0, 0.0, 0.0)

    if n_vals < 2:
        return (float(vals[0]), float(vals[0]), 0.0, float(vals[0]), float(vals[0]))

    # -------------------------------------------------------------
    # 알고리듬 출처: Donald Knuth's
    # "The Art of Computer Programming, Volume 2: Seminumerical Algorithms",
    # section 4.2.2
    #
    # M(1) = x(1), M(k) = M(k-1) + (x(k) - M(k-1)) / k
    # S(1) = 0, S(k) = S(k-1) + (x(k) - M(k-1)) * (x(k) - M(k))
    # for 2 <= k <= n,
    # Sigma = sqrt(S(n) / (n - 1)) ==> Equals to STDEVA(X1~Xn) in Excel
    # ------------------------------------------------------------

    M = float(vals[0]) # MEAN at K'th position
    S = 0.0            # STDEVP at K'th position

    MIN = M
    MAX = M

    for k in range(1, n_vals):
        X = float(vals[k])
        M_1 = M
        M = M + (X - M) / (k+1.0)
        S = S + (X - M_1) * (X - M)
        if (X < MIN): MIN = X
        if (X > MAX): MAX = X

    S = math.sqrt(S / ((float(n_vals)) - 1.0))
    return (M*float(n_vals), M, S, MIN, MAX)
#}

if __name__ == "__main__":
#{
    stat = {}
    for line in sys.stdin.xreadlines():
    #{
        t_ = line.strip().split(":")
        m_ = t_[1].split(".")
        c_ = m_[1].split(",")
        if not stat.has_key(t_[0]):
            stat[t_[0]] = {}
            stat[t_[0]][m_[0]] = {}
        elif not stat[t_[0]].has_key(m_[0]):
            stat[t_[0]][m_[0]] = {}

        stat[t_[0]][m_[0]][c_[0]] = float(c_[1])
    #}

    for t in stat.keys():
    #{
        for m in stat[t].keys():
        #{
            Avect = []
            Fvect = []
            Vect = []
            for c in stat[t][m].keys():
                v = stat[t][m][c]
                Vect.append(v)
                if c[0:1] == "F":
                    Fvect.append(v)
                if c[0:1] == "A":
                    Avect.append(v)

            Uniq = len(Vect)
            (Sum, Mean, Sig, Min, Max) = compute_stats(Vect)

            AUniq = len(Avect)
            (ASum, AMean, ASig, AMin, AMax) = compute_stats(Avect)

            FUniq = len(Fvect)
            (FSum, FMean, FSig, FMin, FMax) = compute_stats(Fvect)

            print "%s, dev:%s, Uniq:%d, Sum:%d, AUniq:%d, ASum:%d, FUniq:%d, FSum:%d" % (t, m, Uniq, Sum, AUniq, ASum, FUniq, FSum)
        #}
    #}
#}
