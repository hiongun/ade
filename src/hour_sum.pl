#!/usr/bin/perl

MAIN:
{
    while (my $line = <STDIN>) {
        $line =~ s/\r?\n//s;
        if ($line =~ /^(\d+):([^\.]+)\.([^,]+),(\d+)$/) {
            my ($hour, $machine, $code, $count) = ($1, $2, $3, int($4));
            $hour = substr($hour, 0, 10) . "0000";
            if (! defined $stat->{"$hour:$machine.$code"}) {
                $stat->{"$hour:$machine.$code"} = $count;
            } else {
                $stat->{"$hour:$machine.$code"} += $count;
            }
        }
    }

    foreach my $key (keys %{$stat}) {
        print "$key,$stat->{$key}\n";
    }
}
