#!/bin/env perl

MAIN:
{
    local(*D);
    if (opendir(D, ".")) {
        while (my $filename = readdir(D)) {
            if ($filename =~ /\.DAT$/ && ! -e "$filename.sort") {
                `sort -u $filename > $filename.sort`;
            }
        }
    }
}
