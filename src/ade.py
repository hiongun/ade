#!/bin/env python
# -*- coding: utf-8 -*-

""" 일반화된 3Sigma에 의한 Anomaly Detector를 구현한 모듈 """

import string
import re
import datetime
import time
import sys
import traceback

import sad  # sad.SimpleAnomalyDetector() 새로 개발된 단순 Anomaly Detector

def DEBUG_PRINT(msg):
    """ displays a warning message with filename and line number """
    fr = sys._getframe(1)
    print "(%s,%d) %s" % (fr.f_code.co_filename, fr.f_lineno, msg.strip())

def test_unit2second():
#{
    assert(sad.unit2second("1m") == 60)
    assert(sad.unit2second("5h") == 60*60*5)
    assert(sad.unit2second("5d") == 60*60*5*24)
#}

def test_YmdHMS2long():
#{
    assert(sad.YmdHMS2long('20150924123049') == int("1443065449"))
#}

def test_long2YmdHMS():
#{
    assert(sad.long2YmdHMS(int("1443065449")) == '20150924123049')
#}


def test_compute_stats():
#{
    vals = [1,2,3,4,5]
    (sum, mean, sig, min, max, uniq) = sad.compute_stats(vals)
    print "vals: ", vals, ", sum: %.1f, mean: %.2f, sig: %.2f, min:%.2f, max:%.2f, uniq:%d" % (sum, mean, sig, min, max, uniq)

    assert(sum == 15.0)
    assert(mean == 3.0)
    assert(1.58 < sig and sig < 1.59)
    assert(min <= 1.0)
    assert(max >= 5.0)
    assert(uniq == 5)

    vals = [1, 0, 1, 1, 2, 1, 4, 2, 1, 19]
    (sum, mean, sig, min, max, uniq) = sad.compute_stats(vals, smoothing=True)
    print "vals: ", vals, ", sum: %.1f, mean: %.2f, sig: %.2f, min:%.2f, max:%.2f, uniq:%d" % (sum, mean, sig, min, max, uniq)

    assert(27.50 <= sum and sum <= 27.60)
    assert(2.75 <= mean and mean <= 2.76)
    assert(4.26 < sig and sig < 4.27)
    assert(min <= 0.0)
    assert(max >= 14.5)
    assert(uniq == 5)
#}



def test_sigmoid():
#{
    print sad.sigmoid(0.5)
    print sad.sigmoid(1.0)
    print sad.sigmoid(1.5)
    print sad.sigmoid(2.0)
    print sad.sigmoid(2.5)
    print sad.sigmoid(600.0)
    print sad.sigmoid(800.0)
    print sad.sigmoid(20000.0)

    print sad.sigmoid(-0.5)
    print sad.sigmoid(-1.0)
    print sad.sigmoid(-1.5)
    print sad.sigmoid(-2.0)
    print sad.sigmoid(-2.5)
    print sad.sigmoid(-600.0)
    print sad.sigmoid(-800.0)
    print sad.sigmoid(-20000.0)
#}

def test_SimpleAnomalyDetector():
#{
    adetect = sad.SimpleAnomalyDetector()
    adetect.set(sad.YmdHMS2long("20150321221200"), 0)
    print adetect.get_stat()
    adetect.detect(sad.YmdHMS2long("20150321221200"))
#}

def test():
#{
    test_unit2second()
    test_SimpleAnomalyDetector()
    test_YmdHMS2long()
    test_long2YmdHMS()
    test_compute_stats()
    test_sigmoid()
#}


def load_machines(filename):
#{
    """이상탐지를 할 머신들의 이름을 로딩하다."""

    machines = []

    fp = open(filename, "r")
    for line in fp.xreadlines():
        machine = line.strip()
        machines.append(machine)

    fp.close()

    return machines
#}


def detect_anomaly(detectors, machines, vars, t):
#{
    if string.find(sad.long2YmdHMS(t), "0000") > 0:
        DEBUG_PRINT("run detect_anomaly() %s" % sad.long2YmdHMS(t))

    for machine in machines:
    #{
        anomaly_count = 0
        anomaly_sum = 0.0

        # DEBUG_PRINT("- machine:%s" % machine)

        for var in vars:
            key = machine + '.' + var

            # DEBUG_PRINT("- key:%s" % key)

            if detectors.has_key(key):
                (Anomaly, Type, SigmaLevel) = detectors[key].detect(t)

                if Anomaly:
                    anomaly_count += 1
                    anomaly_sum += sad.sigmoid(SigmaLevel)
                    print " [detected] %s %s Anomaly:%d SigmaLevels=%.1f,Types=%s" % (machine, sad.long2YmdHMS(t), Anomaly, SigmaLevel, Type)

        if anomaly_count >= 1:
            anomaly_mean = anomaly_sum / float(anomaly_count)
            print "DETECTED machine=%s, t=%s, count:%d, sum:%.2f, mean(sigmoid):%.2f" % (machine, sad.long2YmdHMS(t), anomaly_count, anomaly_sum, anomaly_mean)
    #}
#}



def main():
#{
    machines = load_machines("./machines.txt")
    vars = ["Uniq", "Sum", "AUniq", "ASum", "FUniq", "FSum"]

    assert(len(machines) >= 0)

    detectors = {}
    fp = open("./stat.out", "r")

    prev_time = None

    for line in fp.xreadlines():
    #{
        # print "line: %s" % line
        row = line.strip().split(", ")
        t = sad.YmdHMS2long(row[0])
        dev = row[1][4: ]

        anomal_count = 0
        for i in range(2,len(row)):
            kv = row[i].split(":")

            if not detectors.has_key(dev + "." + kv[0]):
                detectors[dev+'.'+kv[0]] = sad.SimpleAnomalyDetector(name=dev+'.'+kv[0], window_size=3)

            detectors[dev+'.'+kv[0]].set(t, int(float(kv[1])))

        if prev_time == None:
            prev_time = t

        if prev_time != t:
            detect_anomaly(detectors, machines, vars, prev_time)
            prev_time = t

            # DEBUG_PRINT("prev_time: %s" % (prev_time))
    #}

    fp.close()
#}




if __name__ == "__main__":
#{
    test()
    try:
        main()
    except Exception,e:
        print(traceback.format_exc())
#}

