#!/bin/env python
# -*- coding: utf-8 -*-

""" 일반화된 3Sigma에 의한 Anomaly Detector를 구현한 모듈 """

import collections # collections.OrderedDict
import string
import re
import datetime
import time
import math
import sys

def DEBUG_PRINT(msg):
    """ displays a warning message with filename and line number """
    fr = sys._getframe(1)
    print "(%s,%d) %s" % (fr.f_code.co_filename, fr.f_lineno, msg.strip())

class LimitedSizeDict(collections.OrderedDict):
#{
    """일정 사이즈 이상 커지지 않고, 새로운 값이 들어오면, 오래된 것을 지우는 딕트(dict) 구조체"""

    # 메모리에 과거 측정치를 기억해 두고, RandomAccess를 하면서 재계산에 사용

    # vals = LimitedSizeDict(size_limit=s_limit) # 사이즈 크기 제한 초기화 방법
    # vals[x] = y  (Assign방법)

    def __init__(self, *args, **kwds):
    #{
        self.size_limit = kwds.pop("size_limit", None)
        collections.OrderedDict.__init__(self, *args, **kwds)
        self._check_size_limit()
     #}

    def __setitem__(self, key, value):
    #{
        self._check_size_limit()
        collections.OrderedDict.__setitem__(self, key, value)
    #}

    def _check_size_limit(self):
    #{
        if self.size_limit is not None:
            while len(self) > self.size_limit:
                self.popitem(last=False)
    #}
    
#}

def unit2second(val):
#{
    """ 시간 표기를 초단위 수자로 바꾸는 함수 """
    # 12h, 60m, 30s 형태로 쓸 수 있음.
    m = re.search(r'(\d+)(\w+)', val)
    if m == None: return -1

    num = string.atoi(m.group(1), 10)
    unit = m.group(2)

    if unit == 's' or unit == 'S': num = num
    if unit == 'm' or unit == 'M': num = num * 60
    if unit == 'h' or unit == 'H': num = num * 60 * 60
    if unit == 'd' or unit == 'D': num = num * 60 * 60 * 24

    return num
#}

def YmdHMS2long(t_stamp): # 캘린더 시간을 유닉스 시간으로 변환
#{
    # dt = datetime.datetime.strptime(t_stamp,'%Y%m%d%H%M%S')
    t = time.mktime(time.strptime(t_stamp, '%Y%m%d%H%M%S'))
    return int(t)
#}

def long2YmdHMS(t_long):
#{
    return time.strftime("%Y%m%d%H%M%S", time.localtime(t_long))
#}


def compute_stats(vals, smoothing=True):
#{
    """ 과거 데이터(vals)에 대한 통계적 분포값 계산하는 로직, (합, 평균, 표준편차, 최소, 최대, 유니크) 계산' """
    # returns (Sum, Mean, Sigma, Min, Max, UniqCount)

    uniq = set([])
    n_vals = len(vals)
    for v in vals:
        uniq.add(v)

    if n_vals < 1:
        return (0.0, 0.0, 0.0, 0.0, 0.0, len(uniq))

    if n_vals < 2:
        return (float(vals[0]), float(vals[0]), 0.0, float(vals[0]), float(vals[0]), len(uniq))

    # -------------------------------------------------------------
    # 알고리듬 출처: Donald Knuth's
    # "The Art of Computer Programming, Volume 2: Seminumerical Algorithms",
    # section 4.2.2
    #
    # M(1) = x(1), M(k) = M(k-1) + (x(k) - M(k-1)) / k
    # S(1) = 0, S(k) = S(k-1) + (x(k) - M(k-1)) * (x(k) - M(k))
    # for 2 <= k <= n,
    # Sigma = sqrt(S(n) / (n - 1)) ==> Equals to STDEVA(X1~Xn) in Excel
    # ------------------------------------------------------------

    M = float(vals[0]) # MEAN at K'th position
    S = 0.0            # STDEVP at K'th position

    MIN = M
    MAX = M

    for k in range(1, n_vals):
    #{
        X = float(vals[k])
        M_1 = M
        M = M + (X - M) / (k+1.0)
        S = S + (X - M_1) * (X - M)
        if (X < MIN): MIN = X
        if (X > MAX): MAX = X
    #}



    S = math.sqrt(S / ((float(n_vals)) - 1.0))

    if (smoothing): # 2Sigma 밖의 값을 2Sigma이내로 몰아넣어서, Smoothing을 함.
    #{              # Refence없이 실시간 데이터를 바로 처리하기 위함.
        sig = S
        mean = M

        M = float(vals[0])
        if M > (mean+2.0*sig): M = mean+2.0*sig
        if M < (mean-2.0*sig): M = mean-2.0*sig
        S = 0.0            # STDEVP at K'th position

        MIN = M
        MAX = M

        for k in range(1, n_vals):
        #{
            X = float(vals[k])
            if X > (mean+2.0*sig): X = mean+2.0*sig
            if X < (mean-2.0*sig): X = mean-2.0*sig
            M_1 = M
            M = M + (X - M) / (k+1.0)
            S = S + (X - M_1) * (X - M)
            if (X < MIN): MIN = X
            if (X > MAX): MAX = X
        #}

        S = math.sqrt(S / ((float(n_vals)) - 1.0))
    #}

    return (M*float(n_vals), M, S, MIN, MAX, len(uniq))
#}

def sigmoid(x):
#{
    """ 시그모이드 함수 """
    # 예외처리: 너무 큰수, 너무 작은 수에서는 수치 오류 -> 근사치로 수렴
    if (x > 700.0): return 1.0
    if (x < -700.0): return 0.0
    return 1.0 / (1.0 + math.exp(-x))
#}

class SimpleAnomalyDetector(): # 단일변수 Anomaly Detector 클래스
#{
    """특정 변수 하나에 대한 비정상 탐지를 하는 엔진"""

    # ------------------------------------------------------------------------
    # 변수명(name), 최대 데이터 보관기간(time_expire=30d)
    # 데이터 수집 빈도(frequency=5m), 데이터 관찰 윈도우(window_size=3)
    # 짧은 주기 (cycle_short=1d), 긴 주기 (cycle_long=7d)를 줘서
    # 모델을 커스터마이징 할 수 있음.
    # ------------------------------------------------------------------------
    def __init__(self, name="name", time_expire="30d", frequency="5m", window_size=5, cycle_short="24h", cycle_long="7d"):
    #{
        self.name = name
        self.time_expire = unit2second(time_expire)  # 데이터 최대 보관 기간 (30d)
        self.frequency = unit2second(frequency)      # 데이터 입력 주기 (5m)
        self.window_size = window_size               # 과거 데이터를 바라보는 윈도우 (5)
        self.cycle_short = unit2second(cycle_short)  # 짧은 사이클 (1d)
        self.cycle_long = unit2second(cycle_long)    # 긴 사이클 (1w)

        s_limit = int(self.time_expire / self.frequency)

        # self.vals = LimitedSizeDict(size_limit=s_limit)   # 최대 저장용량이 제한된 딕트

        self.keys = collections.deque([], s_limit) # 최대 저장용량이 제한된 큐
        self.vals = collections.deque([], s_limit) # 최대 저장용량이 제한된 큐
    #}

    # 특정 순간에 값을 저장한다.
    def set(self, t, v): # t값에 unix timestamp가 와야 한다. 예: 1443065449
    #{
        # self.vals[t] = v
        # self.vals.append([t,v])
        self.keys.append(t)
        self.vals.append(v)
    #}

    # 특정 순간의 값을 가져온다.
    def get(self, t): # t(unix timestamp)에 해당하는 값을 가져온다.
    #{
        #try:
        #    return self.vals[t]
        #except:
        #    return None

        L, U = 0, len(self.keys)-1  # Binary Search
        while L <= U:
        #{
            M = (L+U)/2
            tM = self.keys[M]
            if tM < t:
                L = M+1
            elif tM > t:
                U = M-1
            else:
                return self.vals[M]
        #}

        return None
    #}

    def summary(self, t):
    #{
        # 집계 기준: 1일 이전 10분 집계, 7일 이전 1시간 집계

        # summary1 = 1d
        # summary2 = 1w
        summary1 = 24 * 60 * 60
        summary2 = 7 * 24 * 60 * 60

        new_keys = []
        new_vals = []
        for key in range(self.keys):
            if key < (t - summary2):
                new_key = (key - (summary2)
    #}

    # 해당 시점(t)에 과거 데이터를 기반으로 이상이 있는지를 판단한다.
    def detect(self, t):
    #{
        """ t시점에 값이, 과거 값에 대비해서, abnomal한 데이터인가? """
        # ---------------------------------------
        # returns (True, Type, SigmaLevel), Type은 3Sig 같은 룰 이름이 나온다
        # ---------------------------------------

        check_v = self.get(t)
        if check_v == None: # 현재 값이 Null이면 무시한다.
            return (False, None, 0.0)


        # 입력주기, 윈도우, 짧은 사이클, 긴 사이클 등을 감안하여,
        # 현재 시점 대비, 관찰할 과거 시점을 복수개 선정한다.
        ticks = []

        # DEBUG_PRINT("self.window_size: %d" % (self.window_size))

        for i in range(1, self.window_size+1):
            ticks.append(t - i*self.frequency)

        if self.cycle_short > 0:
            for i in range(1, self.window_size+1):
                for j in range(0, self.window_size):
                    ticks.append(t - i*self.cycle_short - j*self.frequency)

        if self.cycle_long > 0:
            for i in range(1, self.window_size+1):
                for j in range(0, self.window_size):
                    ticks.append(t - i*self.cycle_short - j*self.frequency)

        if self.cycle_short == 0:
            for i in range(1*self.window_size+1, 2*self.window_size+1):
                ticks.append(t - i*self.frequency)

        if self.cycle_long == 0:
            for i in range(2*self.window_size+1, 3*self.window_size+1):
                ticks.append(t - i*self.frequency)

        ticks.reverse() # 역순, 과거의 데이터에서 현재로 오면서 평균/표준편차 계산



        # 각 관찰 포인트별로, 해당 하는 값을 가져와서 벡터를 구성한다
        vals = []
        for tick in ticks:
            v = self.get(tick)
            if v != None:
                vals.append(v)

        if len(vals) < 9: # 관찰 포인트가 일정수 미만이면 판단을 유보한다.
            return (False, None, 0.0)

        # 과거의 참조값들을 기준으로 통계량(sum, mean, sig, min, max, uniq)를 구한다.
        (sum, mean, sig, min, max, uniq) = compute_stats(vals)

        # 과거 관찰값이 완전히 같은 플랫한 값일 경우, 편차가 무한해 진다.
        # 1.0 미만의 가까이 붙어 있는 점은 정상으로 받아주자.
        if sig == 0.0:
            sig = 0.000000001
            if (float(check_v) - float(mean)) * (float(check_v) - float(mean)) < 2.0:
               return (False, None, 0.0)

        # 2개의 값만 나오는 케이스임.
        # 1.0 미만의 거리로 가까이 붙어 있으면 정상으로 받아주자.
        if uniq <= 2:
            min_diff = 100
            for val in vals:
                if val == check_v:
                    # 과거에 관찰된 값임.
                    return (False, None, 0.0)
                if (check_v - val)*(check_v - val) < min_diff:
                    min_diff = (check_v - val)*(check_v - val)
            if min_diff < 2.0:
                return (False, None, 0.0)

        # 상한선 통과 체크
        if float(check_v) > mean + 3.5 * sig:
            DEBUG_PRINT("%s t=%s vals:" % (self.name, long2YmdHMS(t)) + str(vals) + ", check_v:%d, mean:%.1f, sig:%.1f" % (check_v, mean, sig))
            return (True, '+3Sigma', (float(check_v)-mean)/sig)


        # 하한선 통과 체크
        if float(check_v) < mean - 3.5 * sig:
            DEBUG_PRINT("%s t=%s vals:" % (self.name, long2YmdHMS(t)) + str(vals) + ", check_v:%d, mean:%.1f, sig:%.1f" % (check_v, mean, sig))
            return (True, '-3Sigma', (mean - float(check_v))/sig)

        return (False, '', 0.0)
    #}

    def get_stat(self):
    #{
        stat = "* name: %s\n" % (self.name)
        stat += "* time_expire: %d\n" % (self.time_expire)
        stat += "* frequency: %d\n" % (self.frequency)
        stat += "* window_size: %d\n" % (self.window_size)
        stat += "* cycle_short: %d\n" % (self.cycle_short)
        stat += "* cycle_long: %d\n" % (self.cycle_long)

        #for k in self.vals.keys():
        #    stat += "* [%d: %d]\n" % (k, self.vals[k])

        stat += "."

        return "+OK STAT\n" + stat
    #}
#}



