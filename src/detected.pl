#!/usr/bin/perl

sub sigmoid($)
{
    my ($x) = @_;

    # 시그모이드 함수
    # 예외처리: 너무 큰수, 너무 작은 수에서는 수치 오류 -> 근사치로 수렴
    if ($x > 700.0) { return 1.0; }
    if ($x < -700.0) { return 0.0; }
    return (1.0 / (1.0 + exp(-$x)));
}

sub load_machines()
{
    local(*F);
    my $machines = [];
    open(F, "./machines.txt");
    while (my $line = <F>) {
        $line =~ s/\r?\n//g;
        push @{$machines}, $line;
    }
    return $machines;
}

sub average($)
{
    my ($array) = @_;

    my $sum = 0.0;
    foreach my $item (@{$array}) {
        $sum += $item;
    }
    return $sum / (scalar(@{$array}) + 1);
}


MAIN:
{
    my $machines = load_machines();

    my $stats = {};
    while (my $line = <STDIN>) {
        if ($line =~ /\[detected\] (\S+) (\d+) .+Levels=(\d+)/) {

            my ($machine, $time, $val) = ($1, $2, sprintf("%.5f", $3));

            if ($val > 100) { $val = 99; }

            # print "$1, $2, " . sprintf("%.5f", sigmoid($3)) . "\n";
            if (! defined $stats->{$time}) {
                $stats->{$time} = {};
            }

            if (! defined $stats->{$time}->{$machine}) {
                $stats->{$time}->{$machine} = [];
            }

            push @{$stats->{$time}->{$machine}}, $val;

            # print "line:$line\n";
            # print "val: $val\n";
        }
    }

    my @times = sort {$a cmp $b} (keys %{$stats});



    print "time,";
    foreach my $machine (@{$machines}) {
        print "$machine,";
    }
    print "\n";

    foreach my $time (@times) {
        print "$time,";
        foreach my $machine (@{$machines}) {
            if (defined $stats->{$time}->{$machine}) {

                my $val = average($stats->{$time}->{$machine});

                if ($val > 6.0) {
                    # print scalar(@{$stats->{$time}->{$machine}}) . ",";
                    print int($val) . ",";
                } else {
                    print ",";
                }
            } else {
                print ",";
            }
        }
        print "9\n";
    }
}


