#!/bin/env python
# -*- coding: utf-8 -*-

import string
import re
import datetime
import time
import sys
import traceback

import sad   # Simple Anomaly Detector

def DEBUG_PRINT(msg):
    """ displays a warning message with filename and line number """
    fr = sys._getframe(1)
    print "(%s,%d) %s" % (fr.f_code.co_filename, fr.f_lineno, msg.strip())

def detect_anomaly(detectors, machines, codes, tt):
#{
    if string.find(sad.long2YmdHMS(tt), "000000") > 0:
        DEBUG_PRINT("run detect_anomaly() %s (machines:%d, codes:%d)" % (sad.long2YmdHMS(tt), len(machines), len(codes)))

    for machine in machines:
    #{
        anomaly_count = 0
        anomaly_sum = 0.0

        for code in codes:
            key = machine + '.' + code

            if detectors.has_key(key):
                (AnomalyYes, Type, SigmaLevel) = detectors[key].detect(tt)
                # DEBUG_PRINT("key: %s, Anomaly:%d, Type:%s, SigmaLeve:%.2f\n" % (key, Anomaly, Type, SigmaLevel))

                if AnomalyYes:
                    anomaly_count += 1
                    anomaly_sum += SigmaLevel
                    print " [detected] %s %s SigmaLevel=%.1f,Types=%s" % (machine, sad.long2YmdHMS(tt), SigmaLevel, Type)

        if anomaly_count >= 1:
            anomaly_mean = anomaly_sum / float(anomaly_count)
            print "+ANOMALY-DETECTED machine=%s, t=%s, anomaly_count:%d, mean(SigmaLevel):%.2f" % (machine, sad.long2YmdHMS(tt), anomaly_count, anomaly_mean)
    #}
#}


def main(fname):
#{
    fp = open(fname, "r")
    machines = set([])
    codes = set([])

    detectors = {}  # 여러 개의 sad 객체를 메모리에 가지고 있는 구조체 - 동시에 여러 변수를 추적

    prev_time = None

    p = re.compile(r"(.+):(.+)\.(.+),(.+)")     # 데이터 포맷 20150321001130:MachineName1.FieldName1,Count
    for line in fp.xreadlines():         # STDIN 에서 데이터를 읽어 들인다.
    #{
        match = p.search(line)
        if match == None: continue

        # 라인별 데이터에서, 시각(tt), 기계이름(mm), 변수명(코드명,cc), 카운트값(vv)를 추출한다.
        tt = sad.YmdHMS2long(match.group(1))
        mm = match.group(2)
        cc = match.group(3)
        vv = int(match.group(4))

        key = mm + "." + cc

        if not detectors.has_key(key):
            machines.add(mm)
            codes.add(cc)
            detectors[key] = sad.SimpleAnomalyDetector(name=key, window_size=3, frequency='1h')

        detectors[key].set(tt, vv)

        # 하나의 시간대가 끝나면, 그 시간대에 대해서 anomaly_detect()함수를 돌려서,
        # 한번에 여러 머신의 여러 변수에 대해서 한꺼번에 이상탐지를 수행한다.
        if prev_time != tt:
            if prev_time != None:
                detect_anomaly(detectors, machines, codes, prev_time)
            prev_time = tt
    #}

    fp.close()
#}



if __name__ == "__main__":
#{
    if len(sys.argv) < 2:
        print "Usage: python ade3.py hour_sum.out.sort"
        sys.exit(0)

    fname = sys.argv[1]
    DEBUG_PRINT("fname: %s" % fname)

    main(fname)
#}

