#!/bin/env python
# -*- coding: utf-8 -*-

import math
import sys

def compute_stats(vals):
#{
    """과거 데이터(vals)에 대한 통계적 분포값 계산하는 로직, "합, 평균, 표준편차, MIN, MAX" 계산"""

    n_vals = len(vals)

    if n_vals < 1:
        return (0.0, 0.0, 0.0, 0.0, 0.0)

    if n_vals < 2:
        return (float(vals[0]), float(vals[0]), 0.0, float(vals[0]), float(vals[0]))

    # -------------------------------------------------------------
    # 알고리듬 출처: Donald Knuth's
    # "The Art of Computer Programming, Volume 2: Seminumerical Algorithms",
    # section 4.2.2
    #
    # M(1) = x(1), M(k) = M(k-1) + (x(k) - M(k-1)) / k
    # S(1) = 0, S(k) = S(k-1) + (x(k) - M(k-1)) * (x(k) - M(k))
    # for 2 <= k <= n,
    # Sigma = sqrt(S(n) / (n - 1)) ==> Equals to STDEVA(X1~Xn) in Excel
    # ------------------------------------------------------------

    M = float(vals[0]) # MEAN at K'th position
    S = 0.0            # STDEVP at K'th position

    MIN = M
    MAX = M

    for k in range(1, n_vals):
        X = float(vals[k])
        M_1 = M
        M = M + (X - M) / (k+1.0)
        S = S + (X - M_1) * (X - M)
        if (X < MIN): MIN = X
        if (X > MAX): MAX = X

    S = math.sqrt(S / ((float(n_vals)) - 1.0))
    return (M*float(n_vals), M, S, MIN, MAX)
#}


if __name__ == "__main__":
#{
    stat = {}
    for line in sys.stdin.xreadlines():
    #{
        t_ = line.strip().split(":")
        m_ = t_[1].split(".")
        c_ = m_[1].split(",")

        if not stat.has_key(c_[0]):
            stat[c_[0]] = {}
            stat[c_[0]]['time'] = {}
            stat[c_[0]]['machine'] = {}

        if not stat[c_[0]]['time'].has_key(t_[0]):
            stat[c_[0]]['time'][t_[0]] = float(c_[1])
        else:
            stat[c_[0]]['time'][t_[0]] += float(c_[1])

        if not stat[c_[0]]['machine'].has_key(m_[0]):
            stat[c_[0]]['machine'][m_[0]] = float(c_[1])
        else:
            stat[c_[0]]['machine'][m_[0]] += float(c_[1])
    #}

    for c in stat.keys():
        TVect = []
        for k in stat[c]['time'].keys():
            # print "%s.%s.%s = %.3f" % (c, t, k, stat[c]['time'][k])
            TVect.append(stat[c]['time'][k])
        (TSum, TMean, TSig, TMin, TMax) = compute_stats(TVect)

        MVect = []
        for k in stat[c]['machine'].keys():
            # print "%s.%s.%s = %.3f" % (c, t, k, stat[c]['machine'][k])
            MVect.append(stat[c]['machine'][k])
        (MSum, MMean, MSig, MMin, MMax) = compute_stats(MVect)

        print "%s TCount:%d, TSum:%d, TMean:%.2f, TSig:%.2f, TMin:%.2f, TMax:%.2f, MCount:%d, MSum:%d, MMean:%.2f, MSig:%.2f, MMin:%.2f, MMax:%.2f" % \
           (c, len(TVect), TSum, TMean, TSig, TMin, TMax, len(MVect), MSum, MMean, MSig, MMin, MMax)
#}

